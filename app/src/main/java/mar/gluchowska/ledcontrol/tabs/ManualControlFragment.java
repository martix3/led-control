package mar.gluchowska.ledcontrol.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import mar.gluchowska.ledcontrol.BTLedDevice;
import mar.gluchowska.ledcontrol.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ManualControlFragment extends Fragment {

    protected EditText mMacAddrEditText;
    protected Switch mModeSwitch;

    public ManualControlFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_manual_control, container, false);

        mMacAddrEditText = view.findViewById(R.id.edit_text_mac_input);
        mMacAddrEditText.setText(BTLedDevice.getInstance().getDeviceAddress());
        mMacAddrEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == EditorInfo.IME_ACTION_DONE) || ((event.getKeyCode() == KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))) {

                    String macAddr = mMacAddrEditText.getText().toString();
                    BTLedDevice.getInstance().setDeviceAddress(macAddr);
                }

                return false;
            }
        });

        view.findViewById(R.id.btn_send_string).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.btn_send_string: {
                        EditText editText = getView().findViewById(R.id.edit_text_string);
                        String message = editText.getText().toString();
                        BTLedDevice.getInstance().sendString(getContext(), message);
                        break;
                    }
                }
            }
        });

        mModeSwitch = view.findViewById(R.id.switch_r_t_mode);
        mModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    BTLedDevice.getInstance().sendString(getContext(), "r");
                } else {
                    BTLedDevice.getInstance().sendString(getContext(), "t");
                }
            }
        });

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }
}
