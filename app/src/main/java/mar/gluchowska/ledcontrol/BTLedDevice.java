package mar.gluchowska.ledcontrol;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BTLedDevice {

    private static final BTLedDevice instance = new BTLedDevice();

    public static final int REQUEST_ENABLE_BT = 3;

    // Well known UUID
    private UUID mUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private String mDeviceAddress = null;

    private SharedPreferences mPreferences = null;

    private boolean mConnected = false;
    private BluetoothDevice mDevice;
    private BluetoothSocket mSocket;
    private OutputStream mOutputStream;
    private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    public static BTLedDevice getInstance() {
        return instance;
    }

    private BTLedDevice() {
    }

    public void init(SharedPreferences prefs) {
        mPreferences = prefs;

        mDeviceAddress = mPreferences.getString("DEVICE_ADDRESS", null);

        if (mDeviceAddress == null) {
            setDeviceAddress("98:D3:61:FD:41:0A");
        }

        if (mBluetoothAdapter == null) {
            throw new RuntimeException("Device doesn't Support Bluetooth");
        }

        mDevice = findPairedBtDevice();
    }

    private BluetoothDevice findPairedBtDevice() {

        Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();

        if (bondedDevices.isEmpty() && mBluetoothAdapter.isEnabled()) {
            throw new RuntimeException("Please Pair the Device first");
        } else {
            for (BluetoothDevice iterator : bondedDevices) {

                if (iterator.getAddress().equals(mDeviceAddress)) {
                    return iterator;
                }
            }
        }
        return null;
    }

    public void connect() throws IOException {

        if (!mConnected) {
            mDevice = findPairedBtDevice();

            if (mDevice == null) {
                throw new RuntimeException("Bluetooth device not found");
            }

            mSocket = mDevice.createRfcommSocketToServiceRecord(mUUID);
            mSocket.connect();
            mConnected = mSocket.isConnected();

            if (mConnected) {
                mOutputStream = mSocket.getOutputStream();
            }
        }
    }

    public void disconnect() throws IOException {
        if (mConnected) {
            mSocket.close();
            mConnected = false;

        }
    }

    public boolean isConnected() {
        return mConnected;
    }

    public BluetoothAdapter getBtAdapter() {

        return mBluetoothAdapter;
    }

    public String getDeviceAddress() {
        return mDeviceAddress;
    }

    public void setDeviceAddress(String address) {
        mDeviceAddress = address;
        SharedPreferences.Editor editor = mPreferences.edit();
        editor.putString("DEVICE_ADDRESS", address);
        editor.commit();
    }

    public void sendRgbString(Context ctx, int red, int green, int blue) {
        StringBuilder rgbCommand = new StringBuilder();
        rgbCommand.append(red).append(':').append(green).append(':').append(blue);

        sendString(ctx, rgbCommand.toString());
    }

    public void sendString(Context ctx, String str) {
        if (mConnected) {
            try {
                if (mOutputStream != null) {
                    mOutputStream.write(str.getBytes());
                    Toast.makeText(ctx, str, Toast.LENGTH_SHORT).show();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
