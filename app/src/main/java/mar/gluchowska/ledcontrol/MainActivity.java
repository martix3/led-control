package mar.gluchowska.ledcontrol;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.io.IOException;

import mar.gluchowska.ledcontrol.tabs.ColorPickerFragment;
import mar.gluchowska.ledcontrol.tabs.ManualControlFragment;
import mar.gluchowska.ledcontrol.tabs.TabPagerAdapter;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TabPagerAdapter mTabPagerAdapter;
    private ViewPager mViewPager;
    private FloatingActionButton mConnectBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Set the toolbar
        Toolbar toolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mTabPagerAdapter = new TabPagerAdapter(getSupportFragmentManager());
        mTabPagerAdapter.addFragment(new ColorPickerFragment());
        mTabPagerAdapter.addFragment(new ManualControlFragment());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.tab_pager);
        mViewPager.setAdapter(mTabPagerAdapter);

        mConnectBtn = findViewById(R.id.btn_bt_connection);
        mConnectBtn.setOnClickListener(this);
        mConnectBtn.getBackground().setTint(Color.GRAY);

        // Initialize BT LED device
        SharedPreferences prefs = getSharedPreferences(
                "shared_preferences", Context.MODE_PRIVATE);

        BTLedDevice.getInstance().init(prefs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (BTLedDevice.getInstance().getBtAdapter() != null) {
            if (!BTLedDevice.getInstance().getBtAdapter().isEnabled()) {
                Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableIntent, BTLedDevice.REQUEST_ENABLE_BT);
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            BTLedDevice.getInstance().disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BTLedDevice.REQUEST_ENABLE_BT) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "Bluetooth activated", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Bluetooth not enabled!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.menu_about) {
            Toast.makeText(this, "About clicked", Toast.LENGTH_SHORT).show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

        if (BTLedDevice.getInstance().isConnected()) {
            try {
                BTLedDevice.getInstance().disconnect();
                Toast.makeText(this, "Bluetooth device disconnected", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(e.getMessage())
                        .setTitle(R.string.disconnect_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();

                e.printStackTrace();
            }
            mConnectBtn.getBackground().setTint(Color.GRAY);

        } else {
            try {
                BTLedDevice.getInstance().connect();
                Toast.makeText(this, "Bluetooth device connected", Toast.LENGTH_SHORT).show();
                mConnectBtn.getBackground().setTint(Color.BLUE);
            } catch (Exception e) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setMessage(e.getMessage())
                        .setTitle(R.string.connect_error_title);
                AlertDialog dialog = builder.create();
                dialog.show();

                e.printStackTrace();
                mConnectBtn.getBackground().setTint(Color.GRAY);
            }

        }
    }
}
